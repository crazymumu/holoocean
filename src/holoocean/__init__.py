"""HoloOcean is an underwater robotics simulator.
"""
__version__ = '0.4.1'

from holoocean.holoocean import make
from holoocean.packagemanager import *

__all__ = ['agents', 'environments', 'exceptions', 'holoocean', 'lcm', 'make', 'packagemanager', 'sensors']
